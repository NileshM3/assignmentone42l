package com.example.nileshm.assignmentone42l.Model;

/**
 * Created by NileshM.
 */

public class UserModel {

    private int id;
    private String name;
    private String email;
    private String image;
    private long startTime;
    private long endTime;
    private long randNo;


    public UserModel(int id, String name, String email, String image, long endTime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.image = image;
        this.endTime = endTime;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getRandNo() {
        return randNo;
    }

    public void setRandNo(long randNo) {
        this.randNo = randNo;
    }
}
