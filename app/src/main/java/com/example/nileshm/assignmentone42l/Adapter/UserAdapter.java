package com.example.nileshm.assignmentone42l.Adapter;

import android.app.Activity;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nileshm.assignmentone42l.Activity.MainActivity;
import com.example.nileshm.assignmentone42l.Model.UserModel;
import com.example.nileshm.assignmentone42l.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by NileshM.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private Activity activity;
    private List<UserModel> userModels;


    public UserAdapter(MainActivity mainActivity, List<UserModel> userModels) {
        this.activity = mainActivity;
        this.userModels = userModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int i) {
        final UserModel userModel = userModels.get(i);

        holder.tvName.setText(userModel.getName());
        holder.tvEmail.setText("Email-" + userModel.getEmail());

        Picasso.with(activity).
                load(userModel.getImage())
                .placeholder(R.drawable.user)
                .into(holder.civProfilePic);

        holder.tvEndTime.setText("End Time : " + userModel.getEndTime());
        holder.tvStartTime.setText("Start Time : " + userModel.getStartTime());
        holder.tvCounter.setText("" + userModel.getRandNo() / 1000);

        if (holder.timer != null) {
            holder.timer.cancel();
        }

        holder.timer = new CountDownTimer(userModel.getRandNo(), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //TODO on each interval
                userModel.setRandNo(millisUntilFinished);
                notifyDataSetChanged();
            }

            @Override
            public void onFinish() {
                //TODO on finish
                if (userModels != null && userModels.size() > 0) {
                    MainActivity.tvEmptyText.setVisibility(View.GONE);
                    userModels.remove(userModel);
                } else {
                    MainActivity.tvEmptyText.setVisibility(View.VISIBLE);
                }
                notifyDataSetChanged();
            }
        }.start();

    }

    @Override
    public int getItemCount() {
        return userModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName, tvEmail, tvStartTime, tvEndTime, tvCounter;
        private CircleImageView civProfilePic;
        private CountDownTimer timer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvEmail = (TextView) itemView.findViewById(R.id.tvEmail);
            tvStartTime = (TextView) itemView.findViewById(R.id.tvStartTime);
            tvEndTime = (TextView) itemView.findViewById(R.id.tvEndTime);
            tvCounter = (TextView) itemView.findViewById(R.id.tvCounter);

            civProfilePic = (CircleImageView) itemView.findViewById(R.id.civProfilePic);

        }
    }

}
