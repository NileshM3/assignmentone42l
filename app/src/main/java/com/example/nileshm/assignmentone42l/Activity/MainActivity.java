package com.example.nileshm.assignmentone42l.Activity;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.nileshm.assignmentone42l.Adapter.UserAdapter;
import com.example.nileshm.assignmentone42l.Model.UserModel;
import com.example.nileshm.assignmentone42l.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by NileshM.
 */

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView toolbar_title;
    public static TextView tvEmptyText;
    private RecyclerView recyclerView;

    private UserAdapter userAdapter;
    private List<UserModel> userModels;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

    }

    //Initialising Views
    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.app_name));

        tvEmptyText = (TextView) findViewById(R.id.tvEmptyText);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);


        userModels = new ArrayList<>();
        userModels.add(new UserModel(1, "George Bluth", "george.bluth@gmail.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTIfEg1tbG-YumKz7lba-Q16c1sAtpCmJVqtSwebAA1fTxXFe_q", 120));
        userModels.add(new UserModel(2, "Janet Weaver", "janet.weaver@gmail.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQsg6KXHm6o4SYhPycSwdsJnlGOWCVyIhWBWcg5y8_pki6coDzG", 120));
        userModels.add(new UserModel(3, "Emma Wong", "emma.wong@gmail.com", "https://images.askmen.com/1080x540/2016/01/25-021526-facebook_profile_picture_affects_chances_of_getting_hired.jpg", 120));
        userModels.add(new UserModel(4, "Jane Doe", "jane.doe@gmail.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQjYBVd9MD-pnIIKGjJS1tdx2PiprjCtF7_X1tvNK87PE1ETh7H", 120));
        userModels.add(new UserModel(5, "Sachin Tendulkar", "sachin.tendulkar@gmail.com", "https://images.livemint.com/img/2019/07/11/600x338/2019-06-30T091921Z_924139399_RC1211CCA460_RTRMADP_3_CRICKET-WORLDCUP-ENG-IND_1562829673206_1562829685108.JPG", 120));
        userModels.add(new UserModel(6, "Virat Kohli", "virat.kohli@gmail.com", "https://c.ndtvimg.com/2019-12/klj3qv4o_virat-kohli-afp_625x300_06_December_19.jpg", 120));
        userModels.add(new UserModel(7, "MS Dhoni", "ms.dhoni@gmail.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSeMVTJSc_WYm_SYOIgMHJk4p1jLcAOQ-NQzN2DZGZng3-sxLoP", 120));
        userModels.add(new UserModel(8, "Virendra Sehwag", "virendra.sehwag@gmail.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTh3-A2LLl5fgSGuFRunekTh49yKHEcLVwrww3_2VAyILXz_dTp", 120));

        for (int i = 0; i < userModels.size(); i++) {
            int randNo = generateRandomIntIntRange(30, 120);
            userModels.get(i).setStartTime(120 - randNo);
            userModels.get(i).setRandNo(randNo*1000);
        }

        if (userModels != null && userModels.size() > 0) {
            tvEmptyText.setVisibility(View.GONE);
            userAdapter = new UserAdapter(MainActivity.this, userModels);
            recyclerView.setAdapter(userAdapter);

        } else {
            tvEmptyText.setVisibility(View.VISIBLE);
        }

    }

    //Generating random number
    public static int generateRandomIntIntRange(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

}
